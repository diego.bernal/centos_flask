 === CentOS + httpd + wsgy(py3) + flask ===

Levanta el contianer:

1) Laburamos siempre en un directorio vacio
2) git clone a este proyecto usando esta URL o bajate el zip
3) Construite la image: `docker build -t diego_flask/centos .`
4) Lanza un container: `docker run -dit --name diegotest -p 8084:80 diego_flask/centos`


Informacion de httpd y Flask dentro del container:
```
[root@fee25e6d3c46 app]# tree /var/www/
/var/www/
|-- FlaskApp
|   `-- FlaskApp
|-- cgi-bin
`-- html
    `-- FlaskApp
        |-- FlaskApp
        |   `-- __init__.py
        `-- flaskapp.wsgi
```

```
[root@fee25e6d3c46 app] cat /etc/httpd/conf.d/FlaskApp.conf
<VirtualHost *:80>
                ServerName mywebsite.com
                ServerAdmin admin@mywebsite.com
                WSGIScriptAlias / /var/www/html/FlaskApp/flaskapp.wsgi
                <Directory /var/www/html/FlaskApp/FlaskApp/>
                        Order allow,deny
                        Allow from all
                </Directory>
                Alias /static /var/www/html/FlaskApp/FlaskApp/static
                <Directory /var/www/html/FlaskApp/FlaskApp/static/>
                        Order allow,deny
                        Allow from all
                </Directory>
                ErrorLog /var/log/httpd/error.log
                LogLevel warn
                CustomLog /var/log/httpd/access.log combined
</VirtualHost>
```

